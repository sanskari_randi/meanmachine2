var User = require('../models/user');
var jwt = require('jsonwebtoken');
var config = require('../../config');

var supersecret = config.superSecret;

module.exports = function (app, express) {


    var apiRouter = express.Router();

// route to authenticate a user

    apiRouter.post('/authenticate', function (req, res) {
        // find the user
        User.findOne({
            username: req.body.username
        }).select('name username password').exec(function (err, user) {
            if (err) throw err;

            // if no user with that username is found
            if (!user) {
                res.json({
                    success: false,
                    message: 'Authentication Failed. User not found'
                });
            } else if (user) {
                // check if password matches
                var validPassword = user.comparePassword(req.body.password);
                // if password is wrong
                if (!validPassword) {
                    res.json({
                        success: false,
                        message: 'Authentication failed. Wrong password.'
                    });
                } else {
                    // if user is found and password is right
                    // then create a token
                    var token = jwt.sign({
                        name: user.name,
                        username: user.username
                    }, config.superSecret, {
                        expiresInMinutes: 1440 // expires in 24*60 mints
                    });

                    res.json({
                        success: true,
                        message: 'Enjoy your Token!!',
                        token: token
                    });
                }
            }
        });
    });

// route middleware to verify a token

    apiRouter.use(function (req, res, next) {
        // check post parameter or url parameter or header for token
        var token = req.body.token || req.param('token') || req.headers['x-access-token'];
        //decoding token
        if (token) {  // if there is a token
            // verify secret and checks expiry
            jwt.verify(token, config.superSecret, function (err, decode) {
                if (err) {
                    return res.status(403).send({
                        success: false,
                        message: 'Failed to authenticate Token!'
                    });
                } else {
                    // if everything is good, save the token in the request for use in other routes
                    req.decode = decode;
                    next();
                }
            });
        } else { // if there's no token
            return res.status(403).send({  // send http response of 403 (access forbidden) and an error msg
                success: false,
                message: 'No token provided'
            });
        }
    });

// middleware to use for all requests
    apiRouter.use(function (req, res, next) {
        console.log('SomeBody just came to our App!!');
        next(); // just to make sure we are not stopping here. if we wont use next() app will hand at this point
    });

// / /api home route to test the route
    apiRouter.get('/', function (req, res, next) {
        res.json({message: 'Welcome to our api'});
    });

    apiRouter.route('/users')
        //creating a user
        .post(function (req, res) {
            // create a new instance of user model

            var user = new User();

            // ser the user info that is coming from the POST request
            user.name = req.body.name;
            user.username = req.body.username;
            user.password = req.body.password;

            //save the user and check for the errors
            user.save(function (err) {
                if (err) {
                    //duplicate entry
                    if (err.code == 11000) {
                        return res.json({sucess: false, message: 'A user with that username already exists'});
                    }
                    else
                        return res.send(err);
                }
                res.json({message: 'User Created :)'});
            });
        })
        .get(function (req, res) {
            User.find(function (err, users) {
                // if error then return error
                if (err) return res.send(err);

                //else
                res.json(users);
            });
        });

    apiRouter.route('/users/:user_id')
        //get the user with that id
        .get(function (req, res) {
            User.findById(req.params.user_id, function (err, user) {
                if (err) res.send(err);

                //return that user
                res.json(user);
            });
        })
        //update the user with id
        .put(function (req, res) {
            // use our User model to find the user we want

            User.findById(req.params.user_id, function (err, user) {
                if (err) res.send(err);

                //only update if the details has been changed ,it will check if the name/username/password
                // in request is there , if yes then it will assign to the database saved value

                if (req.body.name)
                    user.name = req.body.name;
                if (req.body.username)
                    user.username = req.body.username;
                if (req.body.password)
                    user.password = req.body.password;

                //save the user
                user.save(function (err) {
                    if (err) res.send(err);
                    res.json({message: 'User updated'});
                });
            });
        })
        //delete the user with id
        .delete(function (req, res) {
            User.remove({
                _id: req.params.user_id
            }, function (err, user) {
                if (err) return res.send(err);
                res.json({message: "User Sucessfully Deleted :("})
            });
        });

// api endpoint to get the user information
    apiRouter.get('/me', function (req, res) {
        res.send(req.decode);
    });

//all routes will be prefixed with the /api
    return apiRouter;
};