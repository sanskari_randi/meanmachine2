var express = require('express');
var morgan = require('morgan');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var jwt = require('jsonwebtoken');
var path = require('path');

var app = express();

var config = require('./config');
var User = require('./app/models/user');
var apiRoutes = require('./app/routes/api')(app, express);


app.use('/api', apiRoutes);


mongoose.connect(config.db, function (err) {
    if(err){
        console.log(err);
    }
    else{
        console.log('Conneted to mongoLab DB');
    }
});

app.use(bodyParser.urlencoded({extended:true}));
app.use(bodyParser.json());


// CORS allows other domain to access/consume our API
app.use(function (req, res, next) {
   res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Method', 'GET, POST');
    res.setHeader('Access-Control-Allow-Headers', 'X-Request-With,content-type, Authorization');
    next();
});

app.use(morgan('dev'));

//basic route for the home page , this is catch all route
app.get('*', function (req, res) {
   res.sendFile(path.join(__dirname + '/public/app/index.html'));
});

//express router instance


app.listen(config.PORT, function (err) {
    if(err){
        console.log(err);
    }
    console.log("Magic happens at port : " + config.PORT );
});